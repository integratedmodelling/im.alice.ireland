private namespace alice.ireland.ecologicalstatus;

/* local concepts */
abstract quality PollutionSensitivity //OrganicMatter
	equals probability of (im:Collapse of biology.nomenclature:MacroInvertebrate ecology:Population) caused by IUPAC:'nitrate phosphate' im:Mass within earth:WaterBody; 

attribute HighlySensitive     describes im:High     PollutionSensitivity;
attribute ModeratelySensitive describes im:Moderate PollutionSensitivity;
attribute MinimallySensitive  describes im:Minimal  PollutionSensitivity; 
/*----------------------------------------------------------------------- */
model local:stefano.balbi:alice.sandbox:bn.status.hs.ept.simple
	    as proportion of HighlySensitive biology.nomenclature:Ept in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)   
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                        named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L                 named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                                 named water_temperature_during_summer,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)     named riparian_broadleaf_forest_proportion_area_of_watershed;

model local:stefano.balbi:alice.sandbox:bn.status.ms.ept.simple
        as proportion of ModeratelySensitive biology.nomenclature:Ept in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)  
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                     named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L              named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                              named water_temperature_during_summer, 
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)  named riparian_broadleaf_forest_proportion_area_of_watershed;

model local:stefano.balbi:alice.sandbox:bn.status.hs.ohc.simple
	    as proportion of HighlySensitive biology.nomenclature:Ohc in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)  
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                        named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L                 named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                                 named water_temperature_during_summer,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)     named riparian_broadleaf_forest_proportion_area_of_watershed; 
		
model local:stefano.balbi:alice.sandbox:bn.status.ms.ohc.simple
        as proportion of ModeratelySensitive biology.nomenclature:Ohc in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)  
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                        named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L                 named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                                 named water_temperature_during_summer,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)     named riparian_broadleaf_forest_proportion_area_of_watershed;

model local:stefano.balbi:alice.sandbox:bn.status.mins.ohc.simple
        as proportion of MinimallySensitive biology.nomenclature:Ohc in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)  
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                        named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L                 named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                                 named water_temperature_during_summer,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)     named riparian_broadleaf_forest_proportion_area_of_watershed;

model local:stefano.balbi:alice.sandbox:bn.status.ms.ni.simple
		as proportion of ModeratelySensitive biology.nomenclature:NonInsect in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)  
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                        named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L                 named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                                 named water_temperature_during_summer,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)     named riparian_broadleaf_forest_proportion_area_of_watershed;

model local:stefano.balbi:alice.sandbox:bn.status.mins.ni.simple
		as proportion of MinimallySensitive biology.nomenclature:NonInsect in (count of  biology.nomenclature:MacroInvertebrate biology:Individual)   
	observing
		ratio of (chemistry.incubation:Nitrate im:Mass) to (IUPAC:Water im:Volume) in mg/L                        named nitrate_mass_to_water_volume_ratio,
		ratio of (chemistry:Suspension physical:Solid im:Mass) to (IUPAC:Water im:Volume) in mg/L                 named solid_suspension_mass_to_water_volume_ratio,
		IUPAC:Water im:Temperature during earth:Summer in Celsius                                                 named water_temperature_during_summer,
		proportion of earth.incubation:Riparian landcover:BroadleafForest in (im:Area of hydrology:Watershed)     named riparian_broadleaf_forest_proportion_area_of_watershed; 
		 
